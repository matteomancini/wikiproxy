# README #


# Wikiproxy


Wikiproxy è un semplice Proxy di Wikipedia, molto brutto ma molto veloce, che permette di inserire una query per effettuare una ricerca perfetta. Il proxy è molto veloce in quanto memorizza in cache gli ultimi articoli cercati e li restituisce senza effettuare richieste a Wikipedia, in un tempo molto minore.
Il servizio, al primo avvio, da il benvenuto all'utente caricando il contenuto della pagina ['Hello World' di wikipedia](https://en.wikipedia.org/wiki/%22Hello,_World!%22_program), dopodichè tutti i successivi articoli cercati verranno visualizzati non appena disponibili, mostrando il tempo di risposta del server.


## Install

    $ npm install

## Usage

	$ node server.js


Copyright (c) 2015 Matteo Mancini <[http://bitbucket.org/matteomancini](http://bitbucket.org/matteomancini)>