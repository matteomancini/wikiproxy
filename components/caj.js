'use strict';
var debug = require('debug')('debug:CapAndJoin');

exports = module.exports = function capAndJoin (query) {
    debug(query);
    var capAndJoined = '';
    var splittedInArray = query.replace('_', ' ').split(' ');
    var connector = '_';
    splittedInArray.forEach(function ( element ) {
      capAndJoined += onlyFirstToUpperCase(element) + connector;
    });
    debug(capAndJoined);
    return capAndJoined.substr(0, (capAndJoined.length - 1));
  };

function onlyFirstToUpperCase ( str ) {
  return str.substr(0, 1).toUpperCase() + str.substr(1).toLowerCase();
}


exports['@literal'] = true;
