'use strict';
var db = require('debug')('cache:setArticle');
var db1 = require('debug')('cache:getArticle');
var db2 = require('debug')('cache:changeTTlArticle');
var NodeCache = require( 'node-cache' );
var Cache = new NodeCache();
var ttl = 120;

exports = module.exports = function () {

  function setArticle (articleName, article) {
    db(articleName);
    db(article);
    Cache.set( articleName, article, 120, function ( err, success ) {
      if ( !err ) {
        db(success + ': New Pair of Key-Value inserted{ key:' + articleName + ' }, expires in ' + ttl + ' sec' );
      }
    });
  }

  function getArticle (articleName) {
    db1(articleName);
    return new Promise(function (resolve, reject) {
      Cache.get( articleName, function ( err, value ) {
        if ( !err ) {
          if (value === undefined) {
            db('There is no Key :' + articleName + ' in cache' );
            resolve(undefined);
          } else {
            resolve(value);
          }
        } else {
          reject(err);
        }
      });
    });
  }


  function changeTtl (articleName) {
    db2(articleName);
    Cache.ttl( articleName, ttl, function ( err, changed ) {
      if ( !err ) {
        db(changed + ' time to live reset');
      }
    });
  }

  return {
    setArticle: setArticle,
    getArticle: getArticle,
    changeTtl: changeTtl
  };

};
exports['@singletone'] = true;
