'use strict';

var cap = require('../capitalizeAndJoin').capAndJoin;
var expect = require('chai').expect;

var app = require('../server');
var request = require('supertest');

var searchFn = require('../searchOnWiki').searchFn;
var cacheFn = require('../searchOnWiki').cacheFn;

describe('testing capitalization', function () {
  it('should capitalize the first char of each word', function () {
    var result = cap('hello world');
    expect(result).to.be.a('string');
    var resultSplit = result.split('_');
    expect(resultSplit).to.be.an('array');
    expect(resultSplit).to.have.length(2);
    expect(resultSplit[0]).to.be.equal('Hello');
    expect(resultSplit[1]).to.be.equal('World');
  });


  it('should accept already capitalized words', function () {
    var result = cap('Hello World');
    expect(result).to.be.a('string');
    var resultSplit = result.split('_');
    expect(resultSplit).to.be.an('array');
    expect(resultSplit).to.have.length(2);
    expect(resultSplit[0]).to.be.equal('Hello');
    expect(resultSplit[1]).to.be.equal('World');
  });


  it('should concatenate words with underscores', function () {
    var result = cap('hello world');
    expect(result).to.be.a('string');
    expect(result).to.be.equal('Hello_World');
  });
});


describe('searchOn wiki returns Wikipedia Article', function () {

  it('should return an article if the query is correct', function (done) {
    request(app).get('/search')
      .query({ q: 'hello world' })
      .expect(200, done);
  });

  describe('perfomance testing', function () {
    var cache = require('../cache');
    var article = 'THIS IS HELLO WORLD!';

    before(function () {
      cache.setArticle('Hello_World', article);
    });

    it('should return fastly if cache is full with requested article', function (done) {
      var initialDate = Date.now();
      request(app).get('/search')
        .query({ q: 'hello world' })
        .expect(200)
        .end(function (err, response) {
          var elapsed = (Date.now()) - initialDate;
          expect(err).to.be.null;
          expect(elapsed).to.be.below(100);
          expect(response.body.message).to.be.equal(article);
          done();
        });
    });

  });


  it('should return an error if requested article is not unique', function (done) {
    request(app).get('/search')
      .query({ q: 'FOOOOKKK' })
      .expect(400, done);
  });

  it('should test only search functionality', function (done) {
    searchFn('hello world')
    .catch(done)
    .then(function (html) {
      expect(html).to.be.a('string');
      done();
    });
  });

  it('should test search and cache functionality without express', function (done) {
    cacheFn('hello world').catch(done)
    .then(function (html) {
      expect(html).to.be.a('string');
      done();
    });
  });

});
