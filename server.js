'use strict';
var debug = require('debug')('index:debug');
var validate = require('express-validation');
var validator = require('./validationSearch');
var ioc = require('./IoC');
var express = require('express');
var app = express();
var nunjucks = require('nunjucks');
var api = {search: ioc.create('api/searchOnWiki')};
var cache = ioc.create('components/cache');
var caj = ioc.create('components/caj');
nunjucks.configure('views', {
  autoescape: true,
  express: app
});

debug(api);
debug(cache);
debug(caj);
app.get('/', api.search);
app.get('/search', validate(validator), api.search);

app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

app.listen(3000, function () {
  console.log('listening on port 3000');
});
