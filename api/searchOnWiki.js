'use strict';
var debug = require('debug')('api:searchOnWiki');
var wikipedia = require('wikipedia-js');
//  var cache = require('./cache');
//  var cAJ = require('./capitalizeAndJoin');





exports = module.exports = function (cache, caj) {
  function searchOnWiki (req, res, next) {
    function searchAndCache (query) {
      return cache.getArticle(query).then(function (result) {
        if (result) {
          cache.changeTtl(query);
          return result;
        }
        return search(query);
      });
    }

    function search (query) {
      var options = {query: query, format: 'html', summaryOnly: true};
      var searchP = new Promise(function (resolve, reject) {
        wikipedia.searchArticle(options, function (err, htmlWikiText) {
          if (err || htmlWikiText == null) return reject(err);
          resolve(htmlWikiText);
        });
      });

      searchP.then(function (html) {
        debug(html);
        cache.setArticle(query, html);
        return html;
      }).catch(function (e) {
        console.log('Error ' + e); // "oh, no!"
      });

      return searchP;
    }

    var query;
    if (req.query.q !== undefined ) {
      query = req.query.q;
    } else {
      query = 'hello world';
    }
    query = caj(query);
    var initialDate = Date.now();
    debug(initialDate);
    searchAndCache(query).then(function (html) {
      if (req.query.q !== undefined) {
        res.status(200).send({ message: html, articleTitle: query, time: (Date.now() - initialDate) });
      }else {
        res.render('body.html', {
          title: query,
          htmlText: html,
          time: (Date.now() - initialDate)
        });
      }
    }).catch( function (err) {
      res.status(400).send({ message: (query + ' Not Found '), articleTitle: ('Error404 - File Not Found'), time: (Date.now() - initialDate) });
    });
  }

  return searchOnWiki;
};

exports['@singleton'] = true;
exports['@require'] = ['components/cache', 'components/caj'];

